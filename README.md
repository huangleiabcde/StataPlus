
## Stata Plus-连老师的 Stata 外部命令集 (2020/5/27)

&emsp;

> Stata连享会 &ensp;   [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0527/175209_6ba564e0_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。

&emsp;

&emsp;


> Stata Plus：连老师的 Stata 外部命令集

## 1. 项目介绍

- 内容：存放了自 2003 一年以来我下载的所有外部命令。
- 更新时间：`2020/5/27 10:23`
- 命令清单：[- 点击查看 -](https://gitee.com/arlionn/StataPlus/blob/master/lian_plus_tree.txt) 

### 项目主页

> 连享会-StataPlus 外部命令集    
> <https://gitee.com/arlionn/StataPlus>       

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-StataPlus主页二维码-草料.png">


&emsp;

## 2. plus.rar 下载链接： 
- **地址1**：
  - 百度云盘  <https://pan.baidu.com/s/1zzrsal0knFoTduOGnN72GA>
  - 提取码：dqe8
- **地址2**：
  - 坚果云  <https://www.jianguoyun.com/p/DR6o7KcQtKiFCBi7-tUC>


&emsp;

## 3. 使用方法

下载 **plus.rar** 后，与你的 plus 文件夹合并或直接覆盖你的 plus 文件夹。

> Note: 这里以 stata15 为例，可以根据自己的版本酌情调整

- **方法1：** 下载 **「plus.rar」** 到本地，解压后，放置于 **D:\stata15\ado** 文件夹下即可。若有自建的 **plus** 文件夹，可以将二者合并，或者直接覆盖 (我的外部命令应该更全面一些)。
- **方法2：** 若想同时保留你自己的 **plus** 和我提供的 **plus** 文件夹，则可以将我的重命名为 **plus2**，然后在 **profile.do** （存放于 `D:\stata15` 目录下）添加如下语句：`adopath + "D:\stata15\ado\plus2"`(绝对路径)，或者 `adopath + "c(sysdir_stata)\ado\plus2"` (相对路径)。重启 Stata 后即可保证 **plus2** 中的命令生效。
- ps，使用过程中可能遇到的问题，都可以在这里找到解答：
  - [[Stata: 外部命令的搜索、安装与使用]](https://www.lianxh.cn/news/c2ab130d9873d.html)
  - [[Stata外部命令：SSC所有外部命令清单]](https://www.lianxh.cn/news/b1070b1f2d42c.html)
  - [[聊聊Stata中的profile文件]](https://www.lianxh.cn/news/ad2b49ad17a3f.html), Stata 中 Profile.do 文档的设定，项目主页 <https://gitee.com/arlionn/StataProfile>




&emsp;

&emsp;

## 附：文件路径相关说明

> plus 文件夹的存放位置

输入 `sysdir` 可以查看你的 plus 文件夹存放于何处。我的文件路径如下：
```stata
. sysdir
   STATA:  D:\stata15\
    BASE:  D:\stata15\ado\base\
    SITE:  D:\stata15\ado\site\
    PLUS:  D:\stata15/ado\plus\
PERSONAL:  D:\stata15/ado\personal\
OLDPLACE:  c:\ado\
```

> Stata 能够识别的 ado 文件存放位置

输入 `adopath` 可以查看 Stata 能够识别的所有 ado 文件的存放位置。如下是我电脑中的设置：

```stata
. adopath
  [1]  (BASE)      "D:\stata15\ado\base/"
  [2]  (SITE)      "D:\stata15\ado\site/"
  [3]              "."
  [4]  (PERSONAL)  "D:\stata15/ado\personal/"
  [5]  (PLUS)      "D:\stata15/ado\plus/"
  [6]  (OLDPLACE)  "c:\ado/"
  [7]              "D:\stata15/\ado\personal\_myado"
```

&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> &#x23E9; **线上直播 9 天**：2020.7.28-8.7  
> &#x1F332; **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> &#x1F34E; **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报竖版CL500.png "连享会：Stata暑期班，9天直播")


&emsp; 


&emsp;

&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)     
> **直播**：2020年5月29-31日；**回放**：可随时购买学习+全套课件    
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://www.lianxh.cn/news/a94e5f6b8df01.html)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-效率专题海报-500.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp;

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, `
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究` 
  - `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`
  - `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`
  - `盈余管理, 特斯拉, 甲壳虫, 论文重现`
  - `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")



--- - --

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

---

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)




